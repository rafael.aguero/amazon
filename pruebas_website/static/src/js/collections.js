odoo.define('pruebas_website.collections', function (require) {
'use strict';
	var CourseGroup = require('pruebas_website.models').CourseGroup;

	var CourseGroupColletion = Backbone.Collection.extend({
	    model: CourseGroup,
	});

	return {
		CourseGroupColletion: CourseGroupColletion,
	}
});