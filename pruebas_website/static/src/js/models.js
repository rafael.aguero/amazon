odoo.define('pruebas_website.models', function (require) {
'use strict';

	var rpc = require('web.rpc');
	var session = require('web.session');

	var AbstractCourseModel = Backbone.Model.extend({
		initialize: function(attributes, options) {
		        Backbone.Model.prototype.initialize.call(attributes);
		        this.course_tmpl_ids = [];
		        this.load_server_data();
		    },
	    models: [],
	    load_server_data: function(){
	        var self = this;
	        var loaded = new $.Deferred();
	        var progress = 0;
	        var progress_step = 1.0 / self.models.length;
	        var tmp = {}; // this is used to share a temporary state between models loaders

	        function load_model(index){
	            if(index >= self.models.length){
	                loaded.resolve();
	            }else{
	                var model = self.models[index];
	                // self.chrome.loading_message(_t('Loading')+' '+(model.label || model.model || ''), progress);

	                var cond = typeof model.condition === 'function'  ? model.condition(self,tmp) : true;
	                if (!cond) {
	                    load_model(index+1);
	                    return;
	                }

	                var fields =  typeof model.fields === 'function'  ? model.fields(self,tmp)  : model.fields;
	                var domain =  typeof model.domain === 'function'  ? model.domain(self,tmp)  : model.domain;
	                var context = typeof model.context === 'function' ? model.context(self,tmp) : model.context || {};
	                var ids     = typeof model.ids === 'function'     ? model.ids(self,tmp) : model.ids;
	                var order   = typeof model.order === 'function'   ? model.order(self,tmp):    model.order;
	                progress += progress_step;

	                if( model.model ){
	                    var params = {
	                        model: model.model,
	                        context: _.extend(context, session.user_context || {}),
	                    };

	                    if (model.ids) {
	                        params.method = 'read';
	                        params.args = [ids, fields];
	                    } else {
	                        params.method = 'search_read';
	                        params.domain = domain;
	                        params.fields = fields;
	                        params.orderBy = order;
	                    }
	                    rpc.query(params).then(function(result){
	                        try{    // catching exceptions in model.loaded(...)
	                            $.when(model.loaded(self,result,tmp))
	                                .then(function(){ load_model(index + 1); },
	                                      function(err){ loaded.reject(err); });
	                        }catch(err){
	                            console.error(err.message, err.stack);
	                            loaded.reject(err);
	                        }
	                    },function(err){
	                        loaded.reject(err);
	                    });
	                }else if( model.loaded ){
	                    try{    // catching exceptions in model.loaded(...)
	                        $.when(model.loaded(self,tmp))
	                            .then(  function(){ load_model(index +1);   },
	                                    function(err){ loaded.reject(err); });
	                    }catch(err){
	                        loaded.reject(err);
	                    }
	                }else{
	                    load_model(index + 1);
	                }
	            }
	        }

	        try{
	            load_model(0);
	        }catch(err){
	            loaded.reject(err);
	        }

	        return loaded;
	    },
	});

	var CourseAppGroup = AbstractCourseModel.extend({
		url: '/save_course_creation',
	    initialize: function(attributes, options){
	        Backbone.Model.prototype.initialize.call(attributes, options);
	        this.course_tmpl_ids = [];
	        this.num_lines = 0;
	        this.date_init = attributes.date_init || '';
	        this.date_finish = attributes.date_finish || '';
	        this.user = null;
	        this.partner_id = null;
	        this.parent_id = null;
	        this.internal_teacher = null;
	        this.load_server_data();
	    },
	    _is_null_name: function(model){

	    	return model.name == null || model.name == undefined;
	    },
	    _is_zero_alumn: function(model){
	    	return model.num_alumn <= 0 || model.num_alumn == undefined;
	    },
	    _any_confirmed: function(model){
	    	return model.confirmed == true;
	    },
	    _any_confirmed_not_date: function(model){
	    	return model.confirmed == true && model.date_init != undefined;
	    },
	    validate: function(attrs, options) {
	    	var self = this;
	    	var models = self.get('groups').models;
	    	if(models.some(self._is_null_name)){
	    		return "No puede tener nombres de lineas vacíos";
	    	}
	    	if(models.some(self._is_zero_alumn)){
	    		return "No puede tener el número de alumnos en 0";
	    	}
	    	var msg = '';
	    	var error = false;
	    	var error_date = false;
	    	_.each(models, function(value, index){	    		
    			if(!value.collection.models.some(self._any_confirmed)){
    				error=true;
    				msg += "<strong>"+value.name+"</strong><br/>";
    			}
    			_.each(value.collection.models, function(item){
					if(item.confirmed && item.date_init == undefined){
						error_date = true;
    				}
    			});


	    	});
	    	if(error){
	    		msg = "Debe tener al menos un curso confirmado en:<br/>" + msg;
	    	}
	    	if(error_date){
	    		msg += "Tiene cursos confirmados sin establecer la fecha.<br/>";
	    	}
	    	if(error || error_date){
	    		return msg;
	    	}
	    },
	    set_num_lines: function(num){
	        this.num_lines = num;
	    },
	    get_num_lines: function(){
	    	return parseInt(this.num_lines);
	    },
	   get_internal_teacher: function(){
	    	return this.internal_teacher;
	    },
	    models: [
	        {
	            model:  'course.template',
	            fields: ['id', 'name','code'],
	            loaded: function(self, course){
	                self.course_tmpl_ids = course;
	            },
	        },
			{
				model:  'res.users',
				fields: ['name','company_id', 'partner_id'],
				ids:    function(self){ return [session.user_id]; },
				loaded: function(self, users){
					self.user = users[0];
					self.partner_id = users[0].partner_id[0];
				},
			},
			{
				model:  'res.partner',
				fields: ['name','parent_id'],
				ids:    function(self){ return [self.partner_id]; },
				loaded: function(self, partner){
					self.parent_id = partner[0].parent_id[0];
				},
			},
			{
				model:  'res.partner',
				fields: ['name'],
				ids:    function(self){ return [self.parent_id]; },
				loaded: function(self, partner){
					self.internal_teacher = partner[0].internal_teacher;
				},
			},	        
	    ],
	});

	var Course = Backbone.Model.extend({
		defaults: function() {
	      return {
	        date_init: new Date(),
	        confirmed: false,
	        internal_teacher: false,
	      };
	    },
	    initialize: function(attributes, options) {
	        Backbone.Model.prototype.initialize.call(attributes);
	        this.course_tmpl_id = attributes.course_tmpl_id || null;
	    },
	    set_date_init: function(date_init){
	    	this.date_init = date_init;
	    },
	    set_confirmed: function(confirmed){
	    	this.confirmed = confirmed;
	    },
	    set_internal_teacher: function(internal_teacher){
	    	this.internal_teacher = internal_teacher;
	    },
	    toJSON: function(){
	    	return {
	    		course_tmpl_id: this.course_tmpl_id,
				confirmed: this.confirmed,
				internal_teacher: this.internal_teacher,
				date_init: this.date_init
	    	}
	    }
	});

	var CourseCollection = Backbone.Collection.extend({
	    model: Course,
	});
		
	var CourseGroup = Backbone.Model.extend({
	    initialize : function(attributes, options) {
	    	Backbone.Model.prototype.initialize.call(attributes, options);
	        this.collection = new CourseCollection();
	        this.name = null;
	        this.num_alumn = 0;	
	    },
	    set_name: function(name){
	    	this.name = name;
	    },
	    set_num_alumn: function(num_alumn){
	    	this.num_alumn = num_alumn;
	    },
	    toJSON: function(){
	    	return {
	    		name: this.name,
				num_alumn: this.num_alumn,
				collection: this.collection.map(function(model){return model.toJSON()})
	    	}
	    }
	});

	return {
		Course: Course,
		CourseGroup: CourseGroup,
		CourseAppGroup: CourseAppGroup,
		CourseCollection: CourseCollection,
	}
});