odoo.define('pruebas_website.views', function (require) {
'use strict';
	var core = require('web.core');
	var time = require('web.time');
	var CourseGroupColletion = require('pruebas_website.collections').CourseGroupColletion;
	var CourseGroup = require('pruebas_website.models').CourseGroup;
	var Course = require('pruebas_website.models').Course;
	var QWeb = core.qweb;
	var ajax = require('web.ajax');
	var Dialog = require('web.Dialog');
	ajax.loadXML('/pruebas_website/static/src/xml/course_js_template.xml', QWeb);
	// QWeb.add_template("/pruebas_website/static/src/xml/course_inscription_template.xml");

	var CourseGroupView = Backbone.View.extend({
		className: 'card',
		initialize: function(){
			this.views = [];
		},
		events: {
			'keyup .name_line': "onChangeLineName",
			'change .num_alumn': "onChangeNumAlum",
		},
	    render: function(options={}){
	        this.$el.append(QWeb.render('CourseInscriptionTemplate', options));
			return this;
	    },
	    onChangeLineName: function(ev){
	    	var val = $(ev.currentTarget)[0].value;
	    	this.model.set_name(val);
	    	this.$el.find('.headerText').html(val);
	    },
	    onChangeNumAlum: function(ev){
	    	var val = $(ev.currentTarget)[0].value;
	    	this.model.set_num_alumn(parseInt(val));
	    }
	});
	var CourseView = Backbone.View.extend({
		events: {
			'change .apply': "onChangeConfirm",
			'change .internal_teacher': "onChangeInternalTeacher",
			'change .datetimepicker-input': "onChangeDateInit",
			'focusout .datetimepicker-input': "onChangeDateInit",
			'changeDate .date': "onChangeDateInit",
		},
		onChangeDateInit: function(ev){
	    	var val = $(ev.currentTarget)[0].value;
	    	if(!val){
	    		return;
	    	}
	    	this.model.set_date_init(val);
	    },
	    onChangeConfirm: function(ev){
	    	var val = $(ev.currentTarget)[0];
	    	this.model.set_confirmed(val.checked);
	    	$(this.$el[0]).find('.datetimepicker-input').trigger('change');
	    },
	    onChangeInternalTeacher: function(ev){
	    	var val = $(ev.currentTarget)[0];
	    	this.model.set_internal_teacher(val.checked);
	    	$(this.$el[0]).find('.datetimepicker-input').trigger('change');
	    },
	    render: function(options={}){
	        this.$el.html(QWeb.render('CourseOneTemplate', options));
	        return this;
	    },	    
	});
	var CourseAccordionView = Backbone.View.extend({

		render: function(){
	        this.$el.append(QWeb.render('CourseCollectionInscriptionTemplate'));
	        return this;
	    },
	});
	var CourseApp = Backbone.View.extend({
	    el: '#div_course_massive_creation',
	    events: {
	        'click #create_massive_course': "onClickCreateLines",
	        'keyup input[name="num_lines"]': "onKeyEnterPress",
	        'click #btnCreateCourse': "clickFinish"
	    },
	    onKeyEnterPress: function(ev){
	    	if(ev.which==13 || ev.keyCode==13 || ev.key=="Enter"){
	    		$('#create_massive_course').trigger('click');
	    	}
	    },
		clickFinish: function(ev){
			var self = this;
			if (this.model.isValid()){
				this.model.save(
					{
						'params': {
							'datas': this.groups.toJSON(), 
							'internal_type_id': this.internal_type_id
						}
					}
				).done(function(result, request, callback){
					if(request == 'success' && result.result){
						var form = $('#formFinish');
						form[0].action = self.url_to_finish;
						form[0].method = 'POST';
						form.submit();
					}
				});
			}
		},
	    onClickCreateLines: function(ev){
	    	var self = this;
	        var $elem = $(ev.currentTarget)
	        .parents('#div_course_massive_creation')
	        .find('input[name="num_lines"]');
	        if(isNaN(parseInt($elem[0].value))){
	        	$(ev.currentTarget)
		        .parents('#div_course_massive_creation')
		        .find('#numero_error').removeClass('invisible');
		        return;
	        }
	        this.url_to_finish = '/college-create-courses/';
	        this.model.set_num_lines(parseInt($elem[0].value));
	        this.model.on("invalid", function(model, error) {
				new Dialog(this, {
				title: 'Error',
				buttons: [{text: 'Cerrar', close: true}],
				$content: "<span>"+error+"</span>",
				}).open();
			});
	        this.templates = this.model.course_tmpl_ids;
	        this.groups = new CourseGroupColletion();
	        this.groups.on("add", this.courseGroupAdd, this);
	        this.accordion = new CourseAccordionView();
	        $('#div_course_massive_creation').html(this.accordion.render().el);
	        for(var i=0; i<this.model.get_num_lines(); i++){
	        	var courseGroup = new CourseGroup();
	        	$(this.templates).each(function(index, value){
                // for(var j=1; j<5; j++){
	        		var newCourse = new Course({course_tmpl_id: value});
		        	courseGroup.collection.add(newCourse);
		    	});
		    	self.groups.add(courseGroup);
		    }
		    this.model.set({groups: this.groups});
	    },
	    courseGroupAdd: function(courseGroup){
	    	var self = this;
	    	if (courseGroup.view == null) {
				courseGroup.view = new CourseGroupView({
					model: courseGroup,					
				});
			}
			var options = {
				headingId: 'heading_' + courseGroup.cid,
				collapseId: courseGroup.cid,
				collapseIdNum: '#'+courseGroup.cid,
				name_line: 'name_line_'+courseGroup.cid,
				name_alumn: 'num_alumn_'+courseGroup.cid,
				is_internal_teacher: self.is_internal_teacher,
				headerText: 'Nuevo( ' + courseGroup.cid + ')'
			};
			$('#accordionCourses').append(courseGroup.view.render(options=options).el);
			courseGroup.collection.forEach(function(course){
				if (course.view == null) {
					course.view = new CourseView({
						model: course,					
					});
					courseGroup.view.views.push(course.view);
				}
				var options = {
					course_tmpl: course.course_tmpl_id,
					apply: 'apply' + course.cid,
					validation_min_date: self.internal_type_init,
					validation_max_date: self.internal_type_finish,
					is_internal_teacher: self.is_internal_teacher,
					internal_teacher: 'internal_teacher' + course.cid,
					date_init: 'date_init' + course.cid,
					date_init_container: 'date_init_container' + course.cid,
					date_init_container_n: '#date_init_container' + course.cid,
				};
				courseGroup.view.$el.find('.card-body').append(course.view.render(options=options).el);
			});
			_.each($('.input-group.date'), function(date_field){
				var minDate = $(date_field).data('mindate') || moment({ y: 1900 });
				var maxDate = $(date_field).data('maxdate') || moment().add(200, "y");
				$('#' + date_field.id).datetimepicker({
					format : time.getLangDatetimeFormat(),
					minDate: minDate,
					maxDate: maxDate,
					calendarWeeks: true,
					daysOfWeekDisabled: [0,6],
					icons: {
						time: 'fa fa-clock-o',
						date: 'fa fa-calendar',
						next: 'fa fa-chevron-right',
						previous: 'fa fa-chevron-left',
						up: 'fa fa-chevron-up',
						down: 'fa fa-chevron-down',
					},
					locale : moment.locale(),
					allowInputToggle: true,
					keyBinds: null,
				});

			});
	    },
	});
	return {
		CourseGroupView:CourseGroupView,
		CourseApp:CourseApp,
	}
});