odoo.define('pruebas_website.pruebas_website', function (require) {
'use strict';

var sAnimations = require('website.content.snippets.animation');
var core = require('web.core');
var Dialog = require('web.Dialog');
var _t = core._t;
var time = require('web.time');
var CourseAppGroup = require('pruebas_website.models').CourseAppGroup;
var CourseApp = require('pruebas_website.views').CourseApp;
var QWeb = core.qweb;
var ajax = require('web.ajax');
ajax.loadXML('/pruebas_website/static/src/xml/course_js_template.xml', QWeb);

sAnimations.registry.WebsiteCourseCreation = sAnimations.Class.extend({
    selector: '.oe_massive_creation_courses',

    /**
     * @constructor
     */
    init: function () {
        this._super.apply(this, arguments);
        this.isWebsite = true;
    },
    /**
     * @override
     */
    start: function () {
        var def = this._super.apply(this, arguments);
        if (this.editableMode) {
            return def;
        }
        window.courseApp = new CourseApp(
            {
                model: new CourseAppGroup(
                    {

                    }
                )
            }
        );
        
        return def;
    },
});

sAnimations.registry.WebsiteCourseEdit = sAnimations.Class.extend({
    selector: '.course_listview',
    read_events: {
        'click .edit-course': 'onClickEdit',
        'click .remove-course': 'onClickRemove',
    },
    onClickRemove: function(ev){
        var ct = $(ev.currentTarget)[0]
        var course_id = ct.getAttribute('data-course-id');
        var options = {
            course_name: ct.getAttribute('data-course-name'),
            date: ct.getAttribute('data-date'),
            course_template: ct.getAttribute('data-coursetmpl'),
        }
        var self = this;
        Dialog.confirm(this, (_t("Seguro?")), {
            confirm_callback: function () {
                self._rpc({
                    route: "/remove_course" ,
                    params: {
                        course_id: course_id,
                    },
                }).then(function(){
                    window.location.reload();
                })
            },
        });
        
    },
    onClickEdit: function(ev){
        var ct = $(ev.currentTarget)[0]
        var course_id = ct.getAttribute('data-course-id');
        var options = {
            course_name: ct.getAttribute('data-course-name'),
            validation_min_date: ct.getAttribute('data-internal-init'),
            validation_max_date: ct.getAttribute('data-internal-finish'),

            date: ct.getAttribute('data-date'),
            course_template: ct.getAttribute('data-coursetmpl'),
        }
        var self = this;
        var $content = $(QWeb.render('Coursepruebas_websiteEditTemplate', options));
        this.dialog = new Dialog(this, {
            title: _t('Actualizar'),
            technical: false,
            buttons: [{text: _t('Salvar'), classes: 'btn-primary', close: true, click: function () {
                var date_init = this.$('.datetimepicker-input').val();
                var course_name = this.$('.course-name').val();
                self._rpc({
                    route: "/update_course_edition" ,
                    params: {
                        course_id: course_id,
                        name: course_name,
                        date_init: date_init
                    },
                }).then(function(){
                    window.location.reload();
                })
            }}, {text: _t('Cancelar'), close: true}],
            $content: $content,
        });
        this.dialog.opened().then(function () {
            var $input_date = self.dialog.$('.datetimepicker-input');
            $input_date.val(ct.getAttribute('data-date'));
            console.log($input_date)
            _.each($('.input-group.date'), function(date_field){
                $('#' + date_field.id).datetimepicker({
                    format : time.getLangDatetimeFormat(),
                    calendarWeeks: true,
                    daysOfWeekDisabled: [0,6],
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        next: 'fa fa-chevron-right',
                        previous: 'fa fa-chevron-left',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                    },
                    allowInputToggle: true,
                    keyBinds: null,
                });

            });
            $input_date.val(ct.getAttribute('data-date'));
        });
        this.dialog.open();
        
    }
   
});

});


