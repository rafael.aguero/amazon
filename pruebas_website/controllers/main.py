# -*- coding: utf-8 -*-

from odoo import http
from odoo.http import request
from odoo.addons.website.controllers.main import QueryURL
from datetime import datetime, timedelta


class Coursepruebas_websiteController(http.Controller):

    @http.route(['''/courses-creations''',
                 ],
                type='http',
                methods=['GET', 'POST'],
                auth="user",
                website=True)
    def courses_creations(self, **kw):
        return request.render(
            "pruebas_website.course_massive_creation_page",
            {}
        )

    @http.route([
        '''/remove_course''',
    ], type='json', methods=['POST'], auth="user", website=True)
    def remove_course(self, **kw):
        course = request.env['course.course'].browse(
            int(kw.get('course_id'))
        )
        return course.unlink()

    @http.route([
        '''/college-create-courses''',
        '''/college-create-courses/page/<int:page>''',
    ],
        type='http', methods=['GET', 'POST'], auth="user", csrf=False,
        website=True)
    def get_list_pruebas_website_course(self, page=0, search='', **kw):
        logged_user = request.env.user

        courses_count = request.env['course.course'].search_count([])
        pager = request.website.pager(
            url='/college-create-courses',
            total=courses_count,
            page=page,
            step=10,
            scope=7,
            url_args=kw
        )
        courses = request.env['course.course'].search(
            [],
            limit=10,
            offset=pager['offset'],
        )
        keep = QueryURL('/college-create-courses')
        render_values = {
            'courses': courses,
            'search': search,
            'pager': pager,
            'page': page,
            'keep': keep,
            'itype': kw.get('internal_type_code', ''),
            'error': False
        }
        return request.render(
            "pruebas_website.college_create_courses_page",
            render_values
        )

    @http.route([
        '''/save_course_creation''',
    ], type='json', methods=['POST'], auth="user", website=True)
    def save_course_creation(self, **kw):
        datas = kw.get('datas')
        Course = request.env['course.course']
        for c in datas:
            for collection in c.get('collection'):
                if collection.get('confirmed', False):
                    Course.create(
                        {
                            'name': '_'.join(
                                [
                                    c.get('name'),
                                    collection.get('course_tmpl_id').get('name')
                                ]
                            ),
                            'date_init': datetime.strptime(
                                collection.get('date_init'), "%d/%m/%Y %H:%M:%S"
                            ) or ''
                        }
                    )

        return True

    @http.route([
        '''/course_pruebas_website_form''',
    ], type='http', methods=['GET', 'POST'], auth="user", website=True)
    def get_form_create_pruebas_website_course(self, **kw):
        template_ids = request.env['course.template'].search([])
        values = {
            'template_ids': template_ids,
        }
        return request.render(
            "pruebas_website.course_course_pruebas_website_form",
            values
        )

    @http.route([
        '''/update_course_edition''',
    ], type='json', methods=['POST'], auth="user", website=True)
    def update_course_edition(self, **kw):
        date_init_str = kw.get('date_init')
        date_init = date_init_str and datetime.strptime(
            date_init_str, "%d/%m/%Y %H:%M:%S"
        ).strftime('%Y-%m-%d') or ''
        course = request.env['course.course'].browse(
            int(kw.get('course_id'))
        )
        course.sudo().write(
            {
                'name': kw.get('name'),
                'date_init': date_init
            }
        )
        return True
