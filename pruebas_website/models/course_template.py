# -*- coding: utf-8 -*-

from odoo import models, api, fields


class CourseTemplate(models.Model):
    _name = 'course.template'

    name = fields.Char()
    code = fields.Char()
    date_init = fields.Date()
    date_finish = fields.Date()
    confirmed = fields.Boolean()
    internal_teacher = fields.Boolean()
