# -*- coding: utf-8 -*-
from odoo import models, api, fields


class CourseCourse(models.Model):
    _name = 'course.course'

    name = fields.Char()
    code = fields.Char()
    date_init = fields.Date()
    date_finish = fields.Date()
    confirmed = fields.Boolean()
    internal_teacher = fields.Boolean()
    course_tmpl_id = fields.Many2one(
        'course.template'
    )
