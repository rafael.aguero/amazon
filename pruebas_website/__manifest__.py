# -*- coding: utf-8 -*-

{
    'name': 'Pruebas website',
    'author': 'quanam',
    'category': 'Extra Tools',
    'summary': "Pruebas.",
    'website': '',
    'description': """
Pruebas
------------------------------------------
This module add courses pruebas_website.
        """,
    'version': '1.0',
    'depends': [
        'website',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/pruebas_website_snippet.xml',
        'views/pruebas_website_template.xml',
        'views/pruebas_website_view.xml',
        'views/pruebas_website_menus.xml',
        'views/pruebas_website_massive_creation_template.xml',
    ],
    'demo': [
    ],
    'installable': True,
    'application': True,
}
#
########################################################################
